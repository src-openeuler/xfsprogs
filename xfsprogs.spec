Name:           xfsprogs
Version:        6.6.0
Release:        7
Summary:        Administration and debugging tools for the XFS file system
License:        GPL+ and LGPLv2+
URL:            https://xfs.wiki.kernel.org

Source0:        http://kernel.org/pub/linux/utils/fs/xfs/xfsprogs/%{name}-%{version}.tar.xz

BuildRequires:  libtool libattr-devel libuuid-devel gcc gettext inih-devel userspace-rcu-devel
BuildRequires:  libedit-devel libblkid-devel >= 2.30 lvm2-devel libicu-devel >= 62.0
Requires:       libedit

Recommends:     %{name}-xfs_scrub
Provides:       xfs-cmds
Obsoletes:      xfs-cmds <= %{version}
Provides:       xfsprogs-qa-devel
Obsoletes:      xfsprogs-qa-devel <= %{version}
Conflicts:      xfsdump < 3.0.1

Patch0:		xfsprogs-5.12.0-default-bigtime-inobtcnt-on.patch
Patch1:		0001-xfsprogs-Add-sw64-architecture.patch
Patch2:		0002-xfs-fix-internal-error-from-AGFL-exhaustion.patch
Patch3:		0003-xfs_db-don-t-hardcode-type-data-size-at-512b.patch
Patch4:		0004-xfs_db-fix-leak-in-flist_find_ftyp.patch
Patch5:		0005-xfs_db-add-helper-for-flist_find_type-for-clearer-fi.patch
Patch6:		0006-xfs_io-fix-mread-with-length-1-mod-page-size.patch
Patch7:		0007-xfs_scrub-don-t-call-phase_end-if-phase_rusage-was-n.patch

%description
xfsprogs are the userspace utilities that manage XFS filesystems.

%package        xfs_scrub
Summary:        xfs filesystem online scrubbing utilities
Requires:       xfsprogs = %{version}-%{release}, python3

%description    xfs_scrub
xfs_scrub is used for checking and repairing metadata in a
mounted XFS filesystem.
WARNING!!! This program is EXPERIMENTAL, which means that its
behavior and interface could change at any time!
xfs_scrub is an immature utility! Do not run this program unless
you have backups of your data!

%package        devel
Summary:        The header files for XFS filesystem
Requires:       xfsprogs = %{version}-%{release}, libuuid-devel

%description    devel
It contains the header files for the  developing of XFS
filesystem.

%package        help
Summary:        Including man files for xfsprogs
Requires:       man
BuildArch:      noarch

%description    help
This contains man files for the using of xfsprogs.


%prep
%autosetup -n %{name}-%{version} -p1

%build
export tagname=CC

%configure --enable-editline=yes --enable-blkid=yes --enable-lto=no

make %{?_smp_mflags}

%install
make DIST_ROOT=%{buildroot} install install-dev \
	PKG_ROOT_SBIN_DIR=%{_sbindir} PKG_ROOT_LIB_DIR=%{_libdir}

rm -f %{buildroot}{%{_lib}/*.{la,a,so},%{_libdir}/*.{la,a}}
rm -rf %{buildroot}%{_datadir}/doc/xfsprogs/

%find_lang %{name}

%ldconfig_scriptlets

%files -f %{name}.lang
%doc doc/CHANGES  README
%{_libdir}/*.so.*
%{_sbindir}/*
%{_unitdir}/*
%{_datadir}/xfsprogs/mkfs/*.conf
%exclude %{_sbindir}/xfs_scrub*
%exclude %{_unitdir}/xfs_scrub*

%files xfs_scrub
%{_sbindir}/xfs_scrub*
%{_libexecdir}/xfsprogs/xfs_scrub*
%{_unitdir}/xfs_scrub*
%{_udevrulesdir}/64-xfs.rules
%{_mandir}/man8/xfs_scrub*
%{_datadir}/xfsprogs/xfs_scrub_all.cron

%files devel
%dir %{_includedir}/xfs
%{_includedir}/xfs/*.h
%{_libdir}/*.so

%files help
%{_mandir}/man*/*
%exclude %{_mandir}/man8/xfs_scrub*



%changelog
* Sat Jul 6 2024 liuh <liuhuan01@kylinos.cn> - 6.6.0-7
- xfs_scrub: don't call phase_end if phase_rusage was not initialized

* Fri Jun 21 2024 liuh <liuhuan01@kylinos.cn> - 6.6.0-6
- sync pathc from community
  xfs_io: fix mread with length 1 mod page size

* Tue Jun 4 2024 liuh <liuhuan01@kylinos.cn> - 6.6.0-5
- sync patches from community

* Wed May 08 2024 chendexi <chendexi@kylinos.cn> - 6.6.0-4
- Remove xfs_scrub related files from the main package to remove python dependencies

* Tue Apr 2 2024 liuh <liuhuan01@kylinos.cn> - 6.6.0-3
- sync patch from community

* Fri Mar 29 2024 liuh <liuhuan01@kylinos.cn> - 6.6.0-2
- sync patch from community

* Tue Feb 27 2024 wuguanghao <wuguanghao3@huawei.com> - 6.6.0-1
- upgrade version to 6.6.0
 
* Mon Jan 8 2024 wuguanghao <wuguanghao3@huawei.com> - 6.5.0-1
- upgrade version to 6.5.0
 
* Wed Dec 27 2023 wuguanghao <wuguanghao3@huawei.com> - 6.1.1-4
- backport patch from community
 
* Wed Jul 19 2023 wuguanghao <wuguanghao3@huawei.com> - 6.1.1-3
- fix compile failure
 
* Fri Apr 28 2023 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 6.1.1-2
- readline has been removed by bbe12eb9a0f9 (xfsprogs: remove libreadline support),
  we can use --enable-editline=yes to enable similar functions.
  fix issue: https://gitee.com/src-openeuler/xfsprogs/issues/I6Y8SU  

* Wed Feb 8 2023 wuguanghao <wuguanghao3@huawei.com> - 6.1.1-1
- upgrade version to 6.1.1

* Sat Nov 19 2022 wuguanghao <wuguanghao3@huawei.com> - 6.0.0-1
- upgrade version to 6.0.0

* Wed Oct 26 2022 wuzx<wuzx1226@qq.com> - 5.14.1-6
- Add sw64 architecture

* Wed Sep 28 2022 Jun Yang <jun.yang@suse.com> - 5.14.1-5
- add Patch3: prevent corruption of passed-in suboption string values

* Thu Aug 18 2022 Xiaole He <hexiaole@kylinos.cn> - 5.14.1-4
- add Patch2: fix inode reservation space for removing transaction 

* Mon Jul 18 2022 Xiaole He <hexiaole@kylinos.cn> - 5.14.1-3
- add Patch1: correct nlink printf specifier from hd to PRIu32

* Fri Jun 10 2022 xiongyu <xiongyu.net@qq.com> - 5.14.1-2
- turn bigtime & inobtcnt on as default

* Mon Dec 6 2021 yanglongkang <yanglongkang@huawei.com> - 5.14.1-1
- update package to 5.14.1

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.9.0-3
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jun 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 5.9.0-2
- add buildrequire gettext.

* Sat Jan 30 2021 yanglongkang <yanglongkang@huawei.com> - 5.9.0-1
- update xfsprogs version to 5.9.0

* Wed Dec 2 2020 lixiaokeng <lixiaokeng@huawei.com> - 5.6.0-3
- backport patch from epoch2

* Wed Nov 25 2020 haowenchao <haowenchao@huawei.com> - 5.6.0-2
- Split xfsprogs-xfs_scrub and the xfsprogs recommends it.

* Thu Jul 16 2020 wuguanghao <wuguanghao3@huawei.com> - 5.6.0-1
- update xfsprogs version to 5.6.0-1

* Wed Aug 28 2019 zhanghaibo <ted.zhang@huawei.com> - 4.17.0-5
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Mon Aug 12 2019 zhanghaibo <ted.zhang@huawei.com> - 4.17.0-4
- Package init
